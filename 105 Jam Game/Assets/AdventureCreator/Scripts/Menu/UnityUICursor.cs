﻿/*
 *
 *	Adventure Creator
 *	by Chris Burton, 2013-2021
 *	
 *	"UnityUICursor.cs"
 * 
 *	This script allows the cursor to be rendered using a Unity UI canvas, allowing for advanced effects such as custom animation.
 * 
 */

using UnityEngine;
using UnityEngine.UI;

namespace AC
{

	/**
	 * This script allows the cursor to be rendered using a Unity UI canvas, allowing for advanced effects such as custom animation.
	 */
	[AddComponentMenu ("Adventure Creator/UI/Unity UI cursor")]
	[HelpURL ("https://www.adventurecreator.org/scripting-guide/class_a_c_1_1_unity_u_i_cursor.html")]
	[RequireComponent (typeof (Canvas))]
	[RequireComponent (typeof (CanvasScaler))]
	public class UnityUICursor : MonoBehaviour
	{

		#region Variables

		[Header ("UI components")]
		[SerializeField] private Texture2D emptyTexture = null;
		[SerializeField] private RawImage rawImageToControl = null;
		[SerializeField] private bool updateImageNativeSize = true;
		[SerializeField] private RectTransform rectTransformToPosition = null;
		private CanvasScaler rootCanvasScaler;
		
		[Header ("Animation (Optional)")]
		[SerializeField] private Animator _animator = null;
		[SerializeField] private string cursorIDIntParameter = "CursorID";
		[SerializeField] private string inventoryIDIntParameter = "InventoryID";
		[SerializeField] private string cursorVisibleBoolParameter = "CursorIsVisible";
		[SerializeField] private string clickTriggerParameter = "Click";

		#endregion


		#region UnityStandards

		private void OnEnable ()
		{
			EventManager.OnSetHardwareCursor += OnSetHardwareCursor;
			GetComponent<Canvas> ().sortingOrder = 100;
			rootCanvasScaler = GetComponent<CanvasScaler> ();
		}


		private void OnDisable ()
		{
			EventManager.OnSetHardwareCursor -= OnSetHardwareCursor;
		}


		private void Update ()
		{
			if (_animator)
			{
				if (!string.IsNullOrEmpty (cursorIDIntParameter)) _animator.SetInteger (cursorIDIntParameter, KickStarter.playerCursor.GetSelectedCursorID ());
				if (!string.IsNullOrEmpty (inventoryIDIntParameter)) _animator.SetInteger (inventoryIDIntParameter, (KickStarter.runtimeInventory.SelectedItem != null) ? KickStarter.runtimeInventory.SelectedItem.id : -1);

				if (Input.GetMouseButtonDown (0))
				{
					if (!string.IsNullOrEmpty (clickTriggerParameter)) _animator.SetTrigger (clickTriggerParameter);
				}
			}

			if (rectTransformToPosition)
			{
				Vector2 _position = KickStarter.playerInput.GetMousePosition ();

				float scalerOffset = 1f;
				if (rootCanvasScaler && rootCanvasScaler.enabled && rootCanvasScaler.uiScaleMode == CanvasScaler.ScaleMode.ScaleWithScreenSize)
				{
					switch (rootCanvasScaler.screenMatchMode)
					{
						case CanvasScaler.ScreenMatchMode.MatchWidthOrHeight:
							float match = rootCanvasScaler.matchWidthOrHeight;
							scalerOffset = (Screen.width / rootCanvasScaler.referenceResolution.x) * (1 - match) + (Screen.height / rootCanvasScaler.referenceResolution.y) * match;
							break;

						case CanvasScaler.ScreenMatchMode.Expand:
							scalerOffset = Mathf.Min (Screen.width / rootCanvasScaler.referenceResolution.x, Screen.height / rootCanvasScaler.referenceResolution.y);
							break;

						case CanvasScaler.ScreenMatchMode.Shrink:
							scalerOffset = Mathf.Max (Screen.width / rootCanvasScaler.referenceResolution.x, Screen.height / rootCanvasScaler.referenceResolution.y);
							break;
					}
				}

				rectTransformToPosition.localPosition = new Vector3 ((_position.x - (Screen.width / 2f)) / scalerOffset, (_position.y - (Screen.height / 2f)) / scalerOffset, rectTransformToPosition.localPosition.z);
			}
		}

		#endregion


		#region CustomEvents

		private void OnSetHardwareCursor (Texture2D texture, Vector2 clickOffset)
		{
			if (_animator && !string.IsNullOrEmpty (cursorVisibleBoolParameter))
			{
				_animator.SetBool (cursorVisibleBoolParameter, (texture != null));

				if (rawImageToControl && texture)
				{
					rawImageToControl.texture = texture;
					if (updateImageNativeSize)
					{
						rawImageToControl.SetNativeSize ();
					}
				}
			}
			else
			{
				if (rawImageToControl)
				{
					rawImageToControl.texture = (texture) ? texture : emptyTexture;
					if (updateImageNativeSize)
					{
						rawImageToControl.SetNativeSize ();
					}
				}
			}
		}

		#endregion

	}

}